 #!/usr/bin/env bash

 # Updates the openvpn files for connecting to nord vpn TCP and UDP servers
 
 # Download Latest Files from NordVPN
wget https://downloads.nordcdn.com/configs/archives/servers/ovpn.zip
unzip -o -d ~/.NordVPN/Nord_openVPN_Servers/ ovpn.zip

# Cleanup
sudo rm ovpn.zip

echo "Updated"
