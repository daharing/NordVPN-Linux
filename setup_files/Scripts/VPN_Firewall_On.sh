#!/usr/bin/env bash

#parameter 1 = DNS IP address
#parameter 2 = tunning interface name (ie tun0)

#save the existing settings so that we can restore them later
if [ ! -f ~/.NordVPN/Backups/iptables.conf_original ]; then
    sudo bash -c "iptables-save > ~/.NordVPN/Backups/iptables.conf_original"
fi

if [ ! -f ~/.NordVPN/Backups/ip6tables.conf_original ]; then
    sudo bash -c "ip6tables-save > ~/.NordVPN/Backups/ip6tables.conf_original"
fi

#disable ipv6 because it is insecure

# If you happen to use X Forwarding through ssh, disabling IPv6 can break this system. To fix that issue, you must open the 
 #etc/ssh/sshd_config file and change the #AddressFamily any to AddressFamily inet. Save that file and restart sshd.

#If you use Postfix, you could encounter issues with the service starting. To fix this, you'll have to use an IPv4 loopback. #Open the /etc/postfix/main.cf file, comment out the localhost line, and add the IPv4 loopback like so:

#inet_interfaces = localhost

#inet_interfaces = 127.0.0.1

if [ -f /etc/sysctl.conf ]; then
        sudo echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf #not sure what this does
        sudo echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
        sudo echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf
        sudo echo "net.ipv6.conf.lo.disable_ipv6 = 1" >> /etc/sysctl.conf
        #sudo echo "net.ipv6.conf.eth0.disable_ipv6 = 1" >> /etc/sysctl.conf
        echo "Modifying /etc/sysctl.conf"
else
    if [ ! -f /etc/sysctl.d/99-sysctl.conf ]; then
        echo "File not found"
    else
        sudo echo "net.ipv4.ip_forward=1" >> /etc/sysctl.d/99-sysctl.conf #not sure what this does
        sudo echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.d/99-sysctl.conf
        sudo echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.d/99-sysctl.conf
        sudo echo "net.ipv6.conf.lo.disable_ipv6 = 1" >> /etc/sysctl.d/99-sysctl.conf
        #sudo echo "net.ipv6.conf.eth0.disable_ipv6 = 1" >> /etc/sysctl.d/99-sysctl.conf
        echo "Modifying /etc/sysctl.d/99-sysctl.conf"
    fi
fi

sudo sysctl -p



#make copy of the iptables configuration files
cp ~/.NordVPN/FirewallConfigs/ipv4Config_template ~/.NordVPN/FirewallConfigs/ipv4Config
cp ~/.NordVPN/FirewallConfigs/ipv6Config_template ~/.NordVPN/FirewallConfigs/ipv6Config

stringDNS="$1"
stringTUN="$2"

#Modify the iptables config files for ipv4
sed -i -e "s/#REPLACE_TUN/$stringTUN/g" ~/.NordVPN/FirewallConfigs/ipv4Config
sed -i -e "s/#REPLACE_DNS/$stringDNS/g" ~/.NordVPN/FirewallConfigs/ipv4Config

#Modify the iptables config files for ipv6
sed -i -e "s/#REPLACE_TUN/$stringTUN/g" ~/.NordVPN/FirewallConfigs/ipv6Config
sed -i -e "s/#REPLACE_DNS/$stringDNS/g" ~/.NordVPN/FirewallConfigs/ipv6Config

#Clean any rules from iptables
sudo iptables -F && sudo iptables -X

#Import new iptables configurations
sudo bash -c "iptables-restore < ~/.NordVPN/FirewallConfigs/ipv4Config"
sudo bash -c "ip6tables-restore < ~/.NordVPN/FirewallConfigs/ipv6Config"

echo "Firewall Setup Complete!"

