#!/usr/bin/env python2

###Please note the command 'sudo wget https://downloads.nordcdn.com/configs/archives/servers/ovpn.zip' to download the latest ovpn files
####or check out the tutorial site at nordvpn: https://nordvpn.com/tutorials/linux/openvpn/

##CONFIGURATION##
    #iterations = 5 #number of times to retrieve server loads/ping times to create an average for picking the final server
writeToFile = False #generate the bash script in addition to printing command?
numberOfPingProcesses = 75 #this is the number of helper processes used to ping servers. The larger the number, the faster the execution but the more system resources used.
ovpn_commandString = "cd ~/.NordVPN/\nsudo ./nordVPN.sh {0} ~/.NordVPN/Nord_openVPN_Servers/ovpn_{2}/{1}.{2}.ovpn" #first brace is for server ip, second is for server domain, 3rd is 'tcp' or 'udp'
#################

## VARIABLES ##
searchParameters = []
serversDataList = []
refinedServersList = []
validFlags = []
validFeatures = []
validCategories = []
category = ""
feature = ""
#validFlags = ['ID', 'CA', 'NL', 'DE', 'HK', 'US', 'GB', 'SE', 'MX', 'BR', 'IT', 'DK', 'RO', 'AT', 'LU', 'NO', 'ZA', 'FI', 'AU', 'SI', 'NZ', 'CR', 'CH', 'HR', 'GE', 'AR', 'IN', 'JP', 'ES', 'FR', 'BE', 'PL', 'LV', 'IL', 'PT', 'CZ', 'TW', 'SG', 'RU', 'UA', 'BG', 'EE', 'TR','BA', 'AE', 'MK', 'HU', 'CY', 'CL', 'MY', 'IS', 'IE', 'AZ', 'GR', 'SK', 'TH', 'EG', 'KR', 'RS', 'VN', 'AL', 'MD']#2 character country codes that have a Nord VPN server
#validFeatures = ['openvpn_tcp', 'openvpn_xor_udp', 'openvpn_tcp_v6', 'proxy_cybersec', 'pptp', 'ikev2', 'socks', 'ikev2_v6', 'openvpn_xor_tcp', 'proxy_ssl_cybersec', 'proxy', 'l2tp', 'openvpn_udp_v6', 'proxy_ssl', 'openvpn_udp']
#validCategories = ['Standard VPN servers', 'P2P', 'Onion Over VPN', 'Double VPN', 'Obfuscated Servers', 'Dedicated IP']
###############

#Get json file data from NordVPN server api
def retrieveJSON():
    import urllib, json
    
    url = "https://nordvpn.com/api/server"
    response = urllib.urlopen(url)
    return json.loads(response.read())

#returns a list of all the supported country codes (i.e. UK)
def getFlags(servers):
    temp = []
    for s in servers:
        if s['flag'] not in temp:
            temp.append(s['flag'])
    return sorted(temp)

#returns a list of all the supported country names (i.e. United Kindom)
def getCorrespondingCountry(servers):
    temp = []
    for s in servers:
        if s['country'] not in temp:
            temp.append(s['country'])
    return sorted(temp)

#returns a list of all the supported Features (i.e. openvpn_tcp)
def getFeatures(servers):
    temp = []
    for s in servers:
        features = s['features']
        for feature in features:
            #print features[feature]
            if features[feature] == True and feature not in temp:
                temp.append(feature)
    return sorted(temp)

#returns a list of all the supported categories (i.e. "Standard VPN servers" or "P2P")
def getCategories(servers):
    temp = []
    for s in servers:
        for c in s['categories']:
            if c['name'] not in temp:
                temp.append(c['name'])
    return sorted(temp)

def getRefinedServerList(serversList, parameters):
    #Search array of dictionaries for search parameters
        # and store matching results in refined variable
    refined = []
    for serverData in serversList:
        result = True
        for (key_name,key_value) in parameters:
            if key_name == "features":
                f = serverData[key_name]
                if f[key_value] == False and (key_name is not "") and (key_value is not ""):
                    result = False
                    break
             
            elif key_name == "categories":
                temp = False
                for c in serverData['categories']:
                    if c['name'] == key_value and (key_name is not "") and (key_value is not ""):
                        temp = True
                        break
                if temp == False:
                    result = False
            else:
                if (key_value not in serverData[key_name]) and (key_name is not "") and (key_value is not ""):
                    result = False
                    break

        if result == True:
            refined.append(serverData)
    return refined

def retrieveUserInput(servers):
    ##Obtain search parameters
    userDefinedParameters = []
    remainingServers = servers

    ##print available flags and get user input choice
    validFlags = getFlags(serversDataList) #get available country flags
    while (validFlags != []):
        print "Please choose one of the following country codes for your desired server location or press enter for any server location:"
        i = 0
        while i < len(validFlags):
            if (i+1)%4 == 0:#this is the number of columns printed
                print validFlags[i]
            else:
                print validFlags[i], "\t",
            i += 1
        flag = raw_input("\n")
        #add user input to the parameters list if not null
        if flag == "":
            break
        elif flag.upper() in validFlags:
            userDefinedParameters.append(["flag", flag.upper()])
            break
        
    if userDefinedParameters != []:
        #Determine the remaining server options after filtering by flag
        remainingServers = getRefinedServerList(remainingServers, userDefinedParameters)

    ##print available features and get user input choice
    validFeatures = getFeatures(remainingServers)
    while (validFeatures != []):
        print "Please enter the number corresponding to the feature you wish to filter for:"# or press enter for any:"
        i = 0
        while i < len(validFeatures):
            print "%d:\t%s" %(i, validFeatures[i])
            i += 1
        feature = raw_input("")
        if (feature.isdigit() and int(feature) >= 0 and int(feature) < len(validFeatures)):
            #add user input to the parameters list if not null
            userDefinedParameters.append(["features", validFeatures[int(feature)]])
            break
        #elif feature == "":
        #    break
    
    #Determine the remaining server options after filtering by features
    if userDefinedParameters != []:
        remainingServers = getRefinedServerList(remainingServers, userDefinedParameters)
    
    ##print available categories and get user input choice
    validCategories = getCategories(remainingServers)
    while(validCategories != []):
        print "Please enter the number corresponding to the category you wish to filter for or press enter for any:"
        i = 0
        while i < len(validCategories):
            print "%d:\t%s" %(i, validCategories[i])
            i += 1
        category = raw_input("")

        if (category.isdigit() and int(category) >= 0 and int(category) < len(validCategories)):
            userDefinedParameters.append(["categories", validCategories[int(category)]])
            break
        elif category == "":
            break
            
    #Determine the remaining server options after filtering by categories
    if userDefinedParameters != []:
        remainingServers = getRefinedServerList(remainingServers, userDefinedParameters)
    return remainingServers, userDefinedParameters


# subprocess.call() is preferred to os.system()
# works under Python 2.7 and 3.4
# works under Linux, Mac OS, Windows
def ping(host):
    """
    Returns True if host responds to a ping request
        or the output of the ping request
    """
    import subprocess, platform

    # Ping parameters as function of OS
    ping_str = "-n 1 -w 1000" if  platform.system().lower()=="windows" else "-c 1 -W 1"
    args = "ping " + " " + ping_str + " " + host
    need_sh = False if  platform.system().lower()=="windows" else True

    # Ping
    #return subprocess.call(args, shell=need_sh) == 0 #this returns true if host responds to ping.

    try:
        pingResult = subprocess.check_output(args, shell=need_sh)#this returns the shell output of the ping command.
        result = pingResult.split("/")
        returnVal = float(result[4])
    except:
        #print pingResult to see what we should actually check for instead of using try statement
        returnVal = -1

    return returnVal

#def pingServer(ip, domain, load, index):
def pingServer(server):
    ip = server[0]
    domain = server[1]
    load = server[2]
    index = server[3]

    pingTime = ping(ip)#ping ip

    if pingTime >= 0:
        print "Pinging server %s/%s%10s%s ms ... Load: %s%%"  %(ip,domain, '..........', pingTime, load)
    else: 
        print "Pinging server %s/%s%10sNo Response"  %(ip,domain, '..........')
    
    return pingTime, index

def getServerPings(serversList):
    from multiprocessing import Pool, TimeoutError
    import time
    import os
    
    if __name__ == '__main__':
        pool = Pool(processes=numberOfPingProcesses)              # start x worker processes

    ipList = []
    maxIndex = len(serversList) - 1
    for index in range(maxIndex, 0-1, -1):
        ipList.append( [serversList[index]['ip_address'], serversList[index]['domain'], serversList[index]['load'], index] )


    # print "[0, 1, 4,..., 81]"
    for (pingTime, index) in pool.map(pingServer, ipList):
        if pingTime >= 0:
            serversList[index]['ping'] = pingTime
        else:
            del serversList[index]

def getServerLoadsHelper_returnPerformanceFactor(server):
    result = 1
    try:
        result = server['performance']
    except:
        result = 1
    return result

def sortServersByPerformanceFactor(serversList):
    if serversList == []:
        return []
    return sorted(serversList,key=getServerLoadsHelper_returnPerformanceFactor)
    
def calculateServersPerformanceFactors(serversList):
    for server in serversList:
        #this calculates some value in which the larger the number, the worse the server is.
        normalizedPing = (server['load'] + 1) / 100.0 #value between 1.01 and 0.01
        normalizedLoad = (server['ping'] / 100.0) # 0 > ping <= 1
        server['performance'] = normalizedLoad * normalizedPing*5.0 #

def generateCommand(server, transportType):
    #pass in a single server dictionary and it will return the copyable 
     #terminal command needed for the VPN connection.
    serverIP = server['ip_address']
    serverDomain = server['domain']
    #print transportType
    if transportType == 'openvpn_udp' or transportType == 'openvpn_xor_udp':
        return ovpn_commandString.format(serverIP, serverDomain, 'udp'), True
    elif transportType == 'openvpn_tcp' or transportType == 'openvpn_xor_tcp':
        return ovpn_commandString.format(serverIP, serverDomain, 'tcp'), True
    else:
        return "No command string has been developed yet for this protocol.", False

def writeFile(fileName, fileContentStr):
    import subprocess, platform
    need_sh = False if  platform.system().lower()=="windows" else True
    
    f = open(fileName, "w") #overwrite existing files
    f.write('#!/usr/bin/env bash\n') #write shebang to file
    f.write(fileContentStr)
    f.close()

    if need_sh == True:
        subprocess.call(["chmod", "u+x", fileName])


## SCRIPT EXECUTION STARTS HERE ###
print "Downloading server load data...."
serversDataList = retrieveJSON()

refinedServersList, inputParameters = retrieveUserInput(serversDataList)

# set what category and feature the user picked
for (name, value) in inputParameters:
    if name == 'categories':
        category = value
    elif name == 'features':
        feature = value

if len(refinedServersList) == 0:
    print "There are no servers that match your search criteria.\n"
    exit()
print "There are %d matching results" %(len(refinedServersList))

print "Further refining based on server load and ping time...."
getServerPings(refinedServersList)
calculateServersPerformanceFactors(refinedServersList)
sortedRefinedServers = sortServersByPerformanceFactor(refinedServersList)

if sortedRefinedServers == []:
    print "\nThere are no available servers!\n"
    exit()

bestServer = sortedRefinedServers[0]
print "\nThe best server is \n%s\tLoad = %d%%, Ping = %s ms\n" %(bestServer['domain'],bestServer['load'], bestServer['ping'])

commandStr, validCmdStr = generateCommand(bestServer, feature)
print "\nCopy command: \n\n", commandStr,"\n\nDone."

if writeToFile == True and validCmdStr == True:
    serverID = bestServer['domain'].split('.')[0]
    name = "vpn{0}.sh".format(serverID.upper())
    writeFile(name, commandStr)
