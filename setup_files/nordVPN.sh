#!/usr/bin/env bash

#parameter 1 = server ip address
#parameter 2 = tcp/udp open vpn file

function killVPN()
    {
    echo "** Trapped CTRL-C"
    sudo ./VPN_Firewall_Off.sh
    sudo ./undo_fixDNS_Leak.sh
    exit 2
    }

# trap ctrl-c and call killVPN ()
#trap killVPN INT
trap "killVPN" 2

if test "$#" -eq 2; then
    cd ~/.NordVPN/Scripts
    #get the vpn server's ip
    sudo ./fixDNS_Leak.sh
    sudo ./VPN_Firewall_On.sh "$1" "tun0"
    sudo openvpn $2

else
echo "Invalid number of parameters!"
fi
